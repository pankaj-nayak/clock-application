TIME MACHINE or CLOCK application to print values every second, minutes and
hours.


STEPS TO RUN : 

1. go run timer.go -duration {"duration in minutes, for which program needs to be run }
  Example : "go run timer.go -duration 120 " --> program will run for 2 hours 
  * Default value for program to run is 2 minutes.
  * If on windows , timer.exe can be run directly to check the results.

2. User can change the values getting printed at the console from the browser,
   Postman application or using any http GET commands with
   query as shown below:
   
   Commands to change second, minute and hour values:
  1. Seconds : "http://localhost:8000/?SECOND="Quack"
  2. Minutes : "http://localhost:8000/?MINUTE="Meeeoooo"  
  3. Hours   : "http://localhost:8000/?HOUR="Quack"
