package main

import (
	"flag"
	"fmt"
	"io"
	"net/http"
	"sync"
	"time"
)

// taking global lock
var lock sync.Mutex

// channel used for setting seconds , minutes and hour values
var dataChannel chan Data

type timeMachine struct {
	second string
	minute string
	hour   string
}
type messageType string

const (
	secondMessage messageType = "SECOND"
	minuteMessage             = "MINUTE"
	hourMessage               = "HOUR"
)

// Data ... Data send to time machine to set second
// minute & hour values
type Data struct {
	messageType messageType
	value       string
}

func (t *timeMachine) SetSecond(value string) {
	lock.Lock()
	t.second = value
	lock.Unlock()
}
func (t *timeMachine) SetMinute(value string) {
	lock.Lock()
	t.minute = value
	lock.Unlock()
}
func (t *timeMachine) SetHour(value string) {
	lock.Lock()
	t.hour = value
	lock.Unlock()
}

// This clock machine will run till it reaches the endTime
//
func (t *timeMachine) StartTimeMachine(endTimeInMinutes time.Duration, wg *sync.WaitGroup) {
	defer wg.Done()
	ticker := time.NewTicker(time.Second)

	done := make(chan bool)

	var seconds, minutes, hours int
	go func() {
		time.Sleep(endTimeInMinutes * time.Minute)
		done <- true
	}()

	for {
		select {
		case <-ticker.C:
			{
				seconds++
				if minutes == 60 && seconds == 60 {
					fmt.Println(t.hour)
					hours++
					minutes = 0
					seconds = 0
					break
				}
				if seconds == 60 {
					minutes++
					// fmt.Println("Tock : ", minutes)
					fmt.Println(t.minute)
					seconds = 0
					break
				}
				fmt.Println(t.second)
			}
		case <-done:
			return
		}
	}

}
func handler(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Hello ")
	for k, v := range r.URL.Query() {
		// fmt.Printf("%s: %s\n", k, v)
		if k == string(secondMessage) {
			data := Data{messageType: secondMessage, value: v[0]}
			dataChannel <- data
		}
		if k == string(minuteMessage) {
			data := Data{messageType: minuteMessage, value: v[0]}
			dataChannel <- data
		}
		if k == string(hourMessage) {
			data := Data{messageType: hourMessage, value: v[0]}
			dataChannel <- data
		}
	}
}

func main() {
	var wg sync.WaitGroup
	dataChannel = make(chan Data)
	timeMachine := timeMachine{second: "tick", minute: "Tock", hour: "Bong"}
	wg.Add(1)

	var endTime int
	flag.IntVar(&endTime, "duration", 2, "Specify the duration for which timeMachine will run in minutes")
	flag.Parse()
	fmt.Printf("duration : %v minutes \n", endTime)
	go timeMachine.StartTimeMachine(time.Duration(endTime), &wg)

	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			select {
			case data := <-dataChannel:
				switch {
				case data.messageType == secondMessage:
					fmt.Println("Setting Second value: ", data.value)
					timeMachine.SetSecond(data.value)
					break
				case data.messageType == minuteMessage:
					fmt.Println("Setting Minute value: ", data.value)
					timeMachine.SetMinute(data.value)
					break
				case data.messageType == hourMessage:
					fmt.Println("Setting Hour value: ", data.value)
					timeMachine.SetHour(data.value)
					break
				}
			}
		}
	}()

	http.HandleFunc("/", handler)
	http.ListenAndServe(":8000", nil)

	wg.Wait()
}
